
describe('Select Dropdown List', () => {

    it('Verify the title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!')
                   .click()
        })
    it('Navigate to Select Dropdown List', () => {

        cy.get('[data-toggle=dropdown]').contains('Input Forms').click()
        cy.get('.dropdown-menu')
            .children('li').eq(3).contains('Select Dropdown List')
            .click()
        cy.contains('Select List Demo')
         })
    it('Test Select List Demo', () => {
        // I select days of week and selected values from the list are display below
        // the dropdown

        cy.get('select#select-demo').should('have.class','form-control')
            .select('Sunday')
        cy.get('.selected-value').contains('Day selected :- Sunday')
        cy.get('select#select-demo').should('have.class','form-control')
            .select('Monday')
        cy.get('.selected-value').contains('Day selected :- Monday')
        cy.get('select#select-demo').should('have.class','form-control')
            .select('Tuesday')
        cy.get('.selected-value').contains('Day selected :- Tuesday')
        cy.get('select#select-demo').should('have.class','form-control')
            .select('Wednesday')
        cy.get('.selected-value').contains('Day selected :- Wednesday')
        cy.get('select#select-demo').should('have.class','form-control')
            .select('Thursday')
        cy.get('.selected-value').contains('Day selected :- Thursday')
        cy.get('select#select-demo').should('have.class','form-control')
            .select('Friday')
        cy.get('.selected-value').contains('Day selected :- Friday')
        cy.get('select#select-demo').should('have.class','form-control')
            .select('Saturday')
        cy.get('.selected-value').contains('Day selected :- Saturday')

    })
    it('Test Multi Select List Demo', () => {
        // I select values from the list and click on 'First Selected' button
        // and it displays value below the buttons

        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : undefined')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : ')
        cy.get('select[name=States]')
            .select('California')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : California')
        cy.get('select[name=States]')
            .select('Florida')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : Florida')
        cy.get('select[name=States]')
            .select('New Jersey')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : New Jersey')
        cy.get('select[name=States]')
            .select('New York')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : New York')
        cy.get('select[name=States]')
            .select('Ohio')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : Ohio')

        cy.get('select[name=States]')
            .select('Texas')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : Texas')
        cy.get('select[name=States]')
            .select('Pennsylvania')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : Pennsylvania')
        cy.get('select[name=States]')
            .select('Washington')
        cy.get('button[id=printMe]').contains('First Selected').click()
        cy.get('.getall-selected')
            .should('have.text','First selected option is : Washington')

        // I select values from the list and click on 'Get all selected' button
        // and it displays value below the buttons
        cy.get('select[name=States]')
            .select('California')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : California')
        cy.get('select[name=States]')
            .select('Florida')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : Florida')
        cy.get('select[name=States]')
            .select('New Jersey')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : New Jersey')
        cy.get('select[name=States]')
            .select('New York')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : New York')
        cy.get('select[name=States]')
            .select('Ohio')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : Ohio')
        cy.get('select[name=States]')
            .select('Texas')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : Texas')
        cy.get('select[name=States]')
            .select('Pennsylvania')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : Pennsylvania')
        cy.get('select[name=States]')
            .select('Washington')
        cy.get('button[id=printAll]').contains('Get All Selected').click()
        cy.get('.getall-selected')
            .should('have.text','Options selected are : Washington')

         })

})










