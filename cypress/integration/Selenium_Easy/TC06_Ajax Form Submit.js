describe('Ajax Form Submit', () => {

    it('Verify title of the page', () => {

        cy.visit('https://www.seleniumeasy.com/test/')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!').click()
    })
    it('Navigate to Ajax Form Submit', () => {

        cy.get('[data-toggle=dropdown]').contains('Input Forms').click()
        cy.get('.dropdown-menu')
            .children('li').eq(5).contains('Ajax Form Submit')
            .click()
        cy.contains('Ajax Form Submit with Loading icon').should('be.visible')
    })
    it('Test Ajax Form Submit with Loading icon', () => {

        // Without typing anything I click on 'Submit' button and beside the 'Name'
        // appears asterisk symbol and the border of input field becomes red
        cy.get('input#btn-submit').should('have.value','submit').click()
        cy.get('.form-group').contains('*')
        cy.get('input')
            .should('have.css', 'border', '1px solid rgb(255, 0, 0)')

        // I type name and comment and after clicking on 'Submit' button it displays,
        // that form submited succesfully
        cy.get('input[id=title]').should('be.visible').type('John')
            .should('have.value','John')
        cy.get('textarea[id=description]').should('be.visible').type('My first comment')
            .should('have.value','My first comment')
        cy.get('input#btn-submit').should('have.value','submit').click()
        cy.wait(2000)
        cy.get('#submit-control').should('have.text','Form submited Successfully!')
    })

})
