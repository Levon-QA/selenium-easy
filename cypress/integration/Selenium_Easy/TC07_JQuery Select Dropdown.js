describe('JQuery Select Dropdown', () => {

    it('Verify title of the page', () => {
//
        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!').click()
    })

    it('Navigate to JQuery Select Dropdown ', () => {

        cy.get('[data-toggle=dropdown]').contains('Input Forms').click()
        cy.get('.dropdown-menu')
            .children('li').eq(6).contains('JQuery Select dropdown')
            .click()
        cy.contains('Single Select - Search and Select country').should('be.visible')
    })
    it('Test Drop Down with Search box', () => {

//I open the dropdown list and select countries
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(1)
            .contains('Australia').click()
        cy.get('#select2-country-container').contains('Australia')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(2)
            .contains('Bangladesh').click()
        cy.get('#select2-country-container').contains('Bangladesh')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(3)
            .contains('Denmark').click()
        cy.get('#select2-country-container').contains('Denmark')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(4)
            .contains('Hong Kong').click()
        cy.get('#select2-country-container').contains('Hong Kong')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(5)
            .contains('India').click()
        cy.get('#select2-country-container').contains('India')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(6)
            .contains('Japan').click()
        cy.get('#select2-country-container').contains('Japan')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(7)
            .contains('Netherlands').click()
        cy.get('#select2-country-container').contains('Netherlands')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(8)
            .contains('New Zealand').click()
        cy.get('#select2-country-container').contains('New Zealand')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(9)
            .contains('South Africa').click()
        cy.get('#select2-country-container').contains('South Africa')
        cy.get('.panel-body').eq(1).click()
        cy.get('.select2-results ul li').eq(10)
            .contains('United States of America').click()
        cy.get('#select2-country-container').contains('United States of America')

        // I open the dropdown list, in search field type first letters
        // of the word and after that select the result
        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('ba')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('Bangladesh').click()


        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('de')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('Denmark').click()


        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('ho')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('Hong Kong').click()


        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('in')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('India').click()


        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('ja')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('Japan').click()


        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('ne')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('Netherlands').click()


        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('new')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('New Zealand').click()


        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('so')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('South Africa').click()

        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('un')
        cy.get('.select2-results')
        cy.get('.select2-results__options').contains('United States of America').click()

        // Negative - I open the dropdown list, in search field type number or
        // the letter,that isn't contain in words and there isn't any result
        cy.get('.select2-selection__arrow').first().click()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('1')
        cy.get('.select2-results ul li').first()
            .contains('No results found')

        cy.get('.select2-selection__arrow').first().dblclick()
        cy.get('input[class=select2-search__field]').eq(1)
            .type('q')
        cy.get('.select2-results ul li').first()
            .contains('No results found')

    })

    it('Test Multi Select - Search and Select multiple states', () => {

        //Negative - I open the dropdown list, in search field type the letter,that
        // isn't contain in words and there isn't any result
        cy.get('.panel-body').eq(2).dblclick()
        cy.get('input[class=select2-search__field]').type('q')
        cy.get('.select2-results ul li').eq(0)
            .contains('No results found')

        // I type 'ca' in search field and select 'California' from select list
        //After that I remove California from the field
        cy.get('.panel-body').eq(2).dblclick()
        cy.get('.select2-results ul li').eq(0).click()
        cy.get('.panel-body').eq(2).click()
        cy.get('input[class=select2-search__field]').type('Ca')
        cy.get('.select2-results ul li').eq(0).click()
        cy.get('.select2-selection__choice__remove').eq(1).dblclick()

    })
    it('Test Drop Down with Disabled values', () => {

        // I open the dropdown list and verify, that there are disabled values
        cy.get('.panel-body').eq(3).click()
        cy.get('.select2-results ul li').eq(1).contains('Guam')
            .should('not.be.enabled')
        cy.get('.panel-body').eq(3).dblclick()
        cy.get('.select2-search__field').eq(1).type('un')
        cy.get('.select2-results ul li').eq(0)
            .contains('United States Minor Outlying Islands')
            .should('not.be.enabled')
    })
    it('Test Drop-down with Category related options', () => {

        // I open dropdown and select options, after that I check the values
        cy.get('select#files').select('Python')
            .should('have.value','jquery')
        cy.get('select#files').select('PHP')
            .should('have.value','jqueryui')
        cy.get('select#files').select('Ruby')
            .should('have.value','jqueryui')
        cy.get('select#files').select('C')
            .should('have.value','jqueryui')
        cy.get('select#files').select('Java')
            .should('have.value','jquery')
        cy.get('select#files').select('.Net')
            .should('have.value','jqueryui')
        cy.get('select#files').select('Unknown Script')
            .should('have.value','somefile')
        cy.get('select#files').select('Other file')
            .should('have.value','someotherfile')

    })

})
