describe('Input Form Submit', () => {

    it('Verify the title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!').click()
    })
    it('Navigate to Input Form Submit', () => {

        cy.get('[data-toggle=dropdown]').contains('Input Forms').click()
        cy.get('.dropdown-menu')
            .children('li').eq(4).contains('Input Form Submit')
            .click()
        cy.contains('Input form with validations').should('be.visible')


            cy.on('uncaught:exception', (err, runnable) => {
                // returning false here prevents Cypress from
                // failing the test
                return false
            })

        //it('Test Input form with validations', () => {

          //I type values in input fields, select the state and check the box,
          // after that click on 'Send' button
        cy.get('input[name=first_name]')
            .should('be.visible').type('John')
            .should('have.value','John')
        cy.get('input[name=last_name]')
            .should('be.visible').type('Smith')
            .should('have.value','Smith')
        cy.get('input[name=email]')
            .should('be.visible').type('example@gmail.com')
            .should('have.value','example@gmail.com')
        cy.get('input[name=phone]')
            .should('be.visible').type('(845)555-1212')
            .should('have.value','(845)555-1212')
        cy.get('input[name=address]')
            .should('be.visible').type('42 West Fairground Ave')
            .should('have.value','42 West Fairground Ave')
        cy.get('input[name=city]')
            .should('be.visible').type('New York')
            .should('have.value','New York')
        cy.get('select[name=state]')
            .select('New York')
            .should('have.value','New York')
        cy.get('input[name=zip]')
            .should('be.visible').type('1510')
            .should('have.value','1510')
        cy.get('input[name=website]')
            .should('be.visible').type('https://www.johnsmith.com')
            .should('have.value','https://www.johnsmith.com')
        cy.get('input[name=hosting]')
            .should('have.value','yes').first().check()
        cy.get('textarea[name=comment]')
            .should('be.visible').type('My new project')
            .should('have.value','My new project')
        cy.get('button[type=submit]').contains('Send ').click()

    })


})

