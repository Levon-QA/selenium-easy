describe('Checkbox Demo', () => {

    it('Verify title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!')
            .click()
    })
    it('Navigate to Checkbox Demo', () => {

        cy.get('[data-toggle=dropdown]').contains('Input Forms').click()
        cy.get('.dropdown-menu')
            .children('li').eq(1).contains('Checkbox Demo')
            .click()
        cy.contains('This would be a basic example to start with checkboxes using selenium')
            .should('be.visible')
    })
    it('Test Single Checkbox Demo', () => {
        // I click on the checkbox and there is success message,that check box is checked

        cy.get('.checkbox').contains('Click on this check box')
        cy.get('input[id=isAgeSelected]')
            .should('be.enabled')
            .check()
            .should('have.checked','checked')
        cy.get('#txtAge').contains('Check box is checked')

        // Afther that I click on the checkbox again and check box is unchecked
        cy.get('.checkbox').contains('Click on this check box')
        cy.get('input[id=isAgeSelected]')
            .should('be.enabled')
            .click()
            .should('not.have.checked')
    })
    it('Test Multiple Checkbox Demo', () => {

        // I check and uncheck 1,2,3,4 options
        cy.get('.checkbox').contains('Option 1')
        cy.get('input[class=cb1-element]').first()
            .should('be.enabled')
            .check()
            .should('have.checked','checked')

        cy.get('.checkbox').contains('Option 1')
        cy.get('input[class=cb1-element]').first()
            .should('be.enabled')
            .click()
            .should('not.have.checked')

        cy.get('.checkbox').contains('Option 2')
        cy.get('input[class=cb1-element]').eq(1)
            .should('be.enabled')
            .check()
            .should('have.checked','checked')

        cy.get('.checkbox').contains('Option 2')
        cy.get('input[class=cb1-element]').eq(1)
            .should('be.enabled')
            .click()
            .should('not.have.checked')

        cy.get('.checkbox').contains('Option 3')
        cy.get('input[class=cb1-element]').eq(2)
            .should('be.enabled')
            .check()
            .should('have.checked','checked')

        cy.get('.checkbox').contains('Option 3')
        cy.get('input[class=cb1-element]').eq(2)
            .should('be.enabled')
            .click()
            .should('not.have.checked')

        cy.get('.checkbox').contains('Option 4')
        cy.get('input[class=cb1-element]').eq(3)
            .should('be.enabled')
            .check()
            .should('have.checked','checked')

        cy.get('.checkbox').contains('Option 4')
        cy.get('input[class=cb1-element]').eq(3)
            .should('be.enabled')
            .click()
            .should('not.have.checked')

        // I click on'Check All' button, options are checked and button changes to 'Uncheck All'
        cy.get('input[id=check1]')
            .should('have.value','Check All')
            .click()
            .should('have.value','Uncheck All')
        cy.get('.cb1-element')
            .should('have.checked','checked')

        // All options are checked and I uncheck option 4 and button changes to 'Check All'
        cy.get('.checkbox').contains('Option 4')
        cy.get('input[class=cb1-element]').eq(3)
            .should('be.enabled')
            .click()
            .should('not.have.checked')
        cy.get('input[id=check1]')
            .should('have.value','Check All')

// Option 4 is unchecked and I uncheck option 3 and after clicking on 'Check All' button
// all options are checked
        cy.get('.checkbox')
        cy.get('input[class=cb1-element]').eq(2)
            .should('be.enabled')
            .click()
            .should('not.have.checked')

        cy.get('input[id=check1]')
            .should('have.value','Check All')
            .click()
        cy.get('input[class=cb1-element]')
            .should('have.checked','checked')
        cy.get('input[id=check1]')
            .should('have.value','Uncheck All')
            .click()
    })

})
