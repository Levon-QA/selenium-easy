describe('Radio Buttons Demo', () => {

    it('Verify title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!')
            .click()
    })
    it('Navigate to Radio Buttons Demo', () => {

        cy.get('[data-toggle=dropdown]').contains('Input Forms').click()
        cy.get('.dropdown-menu')
            .children('li').eq(2).contains('Radio Buttons Demo')
            .click()
        cy.contains('This is again simple example to start working with radio buttons using Selenium')
            .should('be.visible')
    })
    it('Test Radio Button Demo', () => {

        // I click on 'Male' or 'Female' radio button and click on 'Get Checked value'
        // button and there is displayed that 'Male' or 'Female' is checked
        cy.get('input[type=radio]').first()
            .check('Male')
            .should('have.checked','checked')
        cy.get('#buttoncheck').contains('Get Checked value')
            .click()
        cy.get('.radiobutton').contains('Radio button \'Male\' is checked')

        cy.get('input[type=radio]').eq(1)
            .check('Female')
            .should('have.checked','checked')
        cy.get('#buttoncheck').contains('Get Checked value')
            .click()
        cy.get('.radiobutton').contains('Radio button \'Female\' is checked')

    })
    it('Test Group Radio Buttons Demo', () => {
// I check only 'Male' and after clicking on button get the selected value
// only for sex group
        cy.get('input[type=radio]').eq(2)
            .check('Male')
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Male',' Age group: ')

        // I check only 'Male' and after clicking on button get the selected value
        // only for sex group
        cy.get('input[type=radio]').eq(3)
            .check('Female')
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Female',' Age group: ')

        // I check 'Male' and age groups and after clicking on button get the selected
        // values for sex and age groups
        cy.get('input[type=radio]').eq(2)
            .check('Male')
            .should('have.checked','checked')
        cy.get('input[name=ageGroup]').first()
            .check()
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Male',' Age group: 0 - 5')

        cy.get('input[type=radio]').eq(2)
            .check('Male')
            .should('have.checked','checked')
        cy.get('input[name=ageGroup]').eq(1)
            .check()
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Male',' Age group: 5 - 15')

        cy.get('input[type=radio]').eq(2)
            .check('Male')
            .should('have.checked','checked')
        cy.get('input[name=ageGroup]').eq(2)
            .check()
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Male',' Age group: 15 - 50')

        // I check 'Male' and age groups and after clicking on button get the selected
        // values for sex and age groups
        cy.get('input[type=radio]').eq(3)
            .check('Female')
            .should('have.checked','checked')
        cy.get('input[name=ageGroup]').first()
            .check()
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Female',' Age group: 0 - 5')

        cy.get('input[type=radio]').eq(3)
            .check('Female')
            .should('have.checked','checked')
        cy.get('input[name=ageGroup]').eq(1)
            .check()
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Female',' Age group: 5 - 15')

        cy.get('input[type=radio]').eq(3)
            .check('Female')
            .should('have.checked','checked')
        cy.get('input[name=ageGroup]').eq(2)
            .check()
            .should('have.checked','checked')
        cy.get('button[type=button]').contains('Get values')
            .click()
        cy.get('.groupradiobutton')
            .should('contain','Sex : Female',' Age group: 15 - 50')
    })

})
