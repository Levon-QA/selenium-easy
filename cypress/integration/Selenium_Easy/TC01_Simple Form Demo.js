describe('Simple Form Demo', () => {

    it('Verify title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!')
            .click()
    })
    it('Navigate to Simple Form Demo', () => {

        cy.get('[data-toggle=dropdown]').contains('Input Forms').click()
        cy.get('.dropdown-menu')
            .children('li').eq(0).contains('Simple Form Demo')
            .click()
        cy.contains('This would be your first example to start with Selenium')
            .should('be.visible')
    })
    it('Test Single Input Field', () => {
// I enter message and click on the button to display message entered in input field

        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!')
            .click()
        cy.get('input[id=user-message]')
            .should('be.visible').type('Tesvan')
            .should('have.value','Tesvan')
        cy.get('button[type=button]').contains('Show Message')
            .click()
        cy.get('span[id=display]').contains('Tesvan')
    })
    it('Test Two Input Fields', () => {
        // I enter values in A and B input fields and click on the button to display the sum

        cy.get('input[id=sum1]')
            .should('be.visible').type('0')
        cy.get('input[id=sum2]')
            .should('be.visible').type('0')
        cy.get('button[type=button]').contains('Get Total')
            .click()
        cy.get('span[id=displayvalue]').contains('0')

        cy.get('input[id=sum1]')
            .should('be.visible').clear().type('1')
        cy.get('input[id=sum2]')
            .should('be.visible').clear().type('2')
        cy.get('button[type=button]').contains('Get Total')
            .click()
        cy.get('span[id=displayvalue]').contains('3')

        cy.get('input[id=sum1]')
            .should('be.visible').clear().type('-8')
        cy.get('input[id=sum2]')
            .should('be.visible').clear().type('-5')
        cy.get('button[type=button]').contains('Get Total')
            .click()
        cy.get('span[id=displayvalue]').contains('-13')

        cy.get('input[id=sum1]')
            .should('be.visible').clear().type('18')
        cy.get('input[id=sum2]')
            .should('be.visible').clear().type('16')
        cy.get('button[type=button]').contains('Get Total')
            .click()
        cy.get('span[id=displayvalue]').contains(34)

        cy.get('input[id=sum1]')
            .should('be.visible').clear().type('102')
        cy.get('input[id=sum2]')
            .should('be.visible').clear().type('102')
        cy.get('button[type=button]').contains('Get Total')
            .click()
        cy.get('span[id=displayvalue]').contains(/^2\d4$/)

        cy.get('input[id=sum1]')
            .should('be.visible').clear().type('1000000000000000000000')
        cy.get('input[id=sum2]')
            .should('be.visible').clear().type('1000000000000000000000')
        cy.get('button[type=button]').contains('Get Total')
            .click()
        cy.get('span[id=displayvalue]').contains(/^\de\+[0-9]+$/)

        // Negative I enter font instead of a number and display value is Nan

        cy.get('input[id=sum1]')
            .should('be.visible').clear().type('a')
        cy.get('input[id=sum2]')
            .should('be.visible').clear().type('b')
        cy.get('button[type=button]').contains('Get Total').click()
        cy.get('span[id=displayvalue]').contains('NaN')
    })

})
