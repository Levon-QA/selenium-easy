describe('Simple Form Demo', () => {

    it('Verify title of the page ', () => {
        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!')
            .click()
    })
    it('Navigate to Table Pagination', () => {

        cy.get('[data-toggle=dropdown]').contains('Table').click()
        cy.get('.dropdown-menu')
            .children('li').eq(9).contains('Table Pagination')
            .click()
        cy.contains('Table with Pagination Example')
            .should('be.visible')
    })
    it('Test table', () => {

        // I verify, that each page has 6 heading and 5 records
        cy.get('.table-responsive table thead tr')
        cy.get('th').eq(1).contains('Table heading 1')
        cy.get('th').eq(2).contains('Table heading 2')
        cy.get('th').eq(3).contains('Table heading 3')
        cy.get('th').eq(4).contains('Table heading 4')
        cy.get('th').eq(5).contains('Table heading 5')
        cy.get('th').eq(6).contains('Table heading 6')

        cy.get('.table-responsive table tbody tr')
            cy.get('table tbody tr').eq(0)
        cy.get('td').eq(0).contains('1')
        cy.get('.table-responsive table tbody tr').eq(1)
        cy.get('td').eq(7).contains('2')
        cy.get('.table-responsive table tbody tr').eq(3)
        cy.get('td').eq(14).contains('3')
        cy.get('.table-responsive table tbody tr').eq(4)
        cy.get('td').eq(21).contains('4')
        cy.get('.table-responsive table tbody tr').eq(5)
        cy.get('td').eq(28).contains('5')
    })
    it('Test 1,2,3 and Pre,Next buttons', () => {

        // I verify, that Prev and Next buttons enabled to navigate
        cy.get('#myPager li').eq(2).children('a').contains('2')
            .click()
        cy.get('#myPager li').eq(3).children('a').contains('3')
            .click()
        cy.get('#myPager li').eq(1).children('a').contains('1')
            .click()
        cy.get('#myPager li').eq(4).children('a').contains('»')
            .click()
        cy.get('#myPager li').eq(0).children('a').contains('«')
            .click()
    })
})
