describe('Table Data Search', () => {

    it('Verify title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!')
            .click()
    })
    it('Navigate to Table Pagination', () => {

        cy.get('[data-toggle=dropdown]').contains('Table').click()
        cy.get('.dropdown-menu')
            .children('li').eq(11)
            .contains('Table Filter')
            .click()
        cy.contains('Table Filter Demo')
            .should('be.visible')
    })
    it('Test Table Filter Demo',() => {

        // I filter records by color,by clicking on color buttons
        cy.get('.btn-group button').eq(0).click()
        cy.get('.table-container tbody tr').eq(0)
        cy.get('.media-body').eq(0)
            .children('h4,span').contains('Green')
        cy.get('.table-container tbody tr').eq(3)
        cy.get('.media-body').eq(3)
            .children('h4,span').contains('Green')
        cy.get('.btn-group button').eq(1).click()
        cy.get('.table-container tbody tr').eq(0)
        cy.get('.media-body').eq(1)
            .children('h4,span').contains('Orange')
        cy.get('.table-container tbody tr').eq(4)
        cy.get('.media-body').eq(4)
            .children('h4,span').contains('Orange')
        cy.get('.btn-group button').eq(2).click()
        cy.get('.table-container tbody tr').eq(0)
        cy.get('.media-body').eq(2)
            .children('h4,span').contains('Red')

    })
})
