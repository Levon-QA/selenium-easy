describe('JQuery Date Picker Demo', () => {

    it('Verify title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!').click()

    })
    it('Navigate to JQuery Date Picker page', () => {

        cy.get('[data-toggle=dropdown]').contains('Date pickers').click()
        cy.get('.dropdown-menu')
            .children('li').eq(8).contains('JQuery Date Picker')
            .click()
        cy.contains('JQuery Date Picker Demo').should('be.visible')

    })
    it('Test Date Picker buttons', () => {

       // I open datepicker 'from' field and select months
        cy.get('#from').click()
        cy.get('#ui-datepicker-div div').eq(0)
            .children('a').eq(0).click()
        cy.get('#ui-datepicker-div div div select option').eq(0)
            .contains('Jan')
        cy.get('#ui-datepicker-div div').eq(0)
            .children('a').eq(1).click()
        cy.get('#ui-datepicker-div div div select option').eq(1)
            .contains('Feb')
        cy.get('#ui-datepicker-div div div select').select('May')
        cy.get('#ui-datepicker-div div div select option').eq(4)
            .contains('May').should('have.attr','selected')
        cy.get('#ui-datepicker-div div div select').select('Feb')
        cy.get('#ui-datepicker-div div div select option').eq(1)
            .contains('Feb').should('have.attr','selected')
    })
    it('Test if start date is less than end date', () => {

        // I click on 'from' field and select 'March 18',after that I click on
        // 'to' field to verify,that previous dates disabled
        cy.get('#from').click()
        cy.get('.ui-datepicker-calendar tbody tr').eq(2)
            .children('td').eq(4).contains('18').click()
        cy.get('#to').click()
        cy.get('.ui-datepicker-calendar tbody tr').eq(2)
            .children('td').eq(0).contains('14')
            .should('not.be.enabled')
        cy.get('tbody tr').eq(2)
            .children('td').eq(1).contains('15')
            .should('not.be.enabled')
        cy.get('tbody tr').eq(2)
            .children('td').eq(2).contains('16')
            .should('not.be.enabled')
        cy.get('tbody tr').eq(2)
            .children('td').eq(3).contains('17')
            .should('not.be.enabled')
        cy.get('tbody tr').eq(2)
            .children('td').eq(5).contains('19')
            .click()
        cy.get('#from').click()
        cy.get('.ui-datepicker-calendar tbody tr').eq(2)
            .children('td').eq(6)
            .contains('20')
            .should('not.be.enabled')

    })
})
