describe('Bootstrap Date Picker', () => {

    it('Verify title of the page', () => {

        cy.visit('')
        cy.title().should('include', 'Selenium Easy')
        cy.get('div#at-cv-lightbox-button-holder').contains('No, thanks!').click()
    })
    it('Navigate to Bootstrap Date Picker', () => {

        cy.get('[data-toggle=dropdown]').contains('Date pickers').click()
        cy.get('.dropdown-menu')
            .children('li').eq(7).contains('Bootstrap Date Picker')
            .click()
        cy.contains('Bootstrap Date Pickers Example')
            .should('be.visible')

    })

    it('Test if Sunday day disabled', () => {

        // I open the datepicker, click on '14'(Sunday day) and see, that
        // the day disabled
        cy.get('.input-group-addon').first().click()
        cy.get('.datepicker-days tr').eq(5)
            .should('contain','14').children('td').eq(6)
            .click()
            .should('not.be.enabled')
            .should('have.class','disabled disabled-date day')
    })
    it('Test if future dates disabled', () => {

        // I open the datepicker, click on futures days and see, that they disabled
        cy.get('.input-group-addon').first().click()
        cy.get('.datepicker-days tr').eq(7)
            .should('contain','25').children('td').eq(3)
            .should('not.be.enabled')
        cy.get('tr').eq(7)
            .should('contain','26').children('td').eq(4)
            .should('not.be.enabled')
        cy.get('tr').eq(7)
            .should('contain','27').children('td').eq(5)
            .should('not.be.enabled')
        cy.get('tr').eq(7)
            .should('contain','28').children('td').eq(6)
            .should('not.be.enabled')
            .should('have.class','disabled disabled-date day')
    })
    it('Test if week starts from Monday', () => {

        // I open the datepicker and verify, that week starts from Monday
        cy.get('.input-group-addon').first()
            .click()
        cy.get('.datepicker-days tr').eq(2)
            .should('contain','Mo').children('th').eq(0)
    })
    it('Test click on Clear button to clear the date entered', () => {

        // I open the datepicker and verify, that there is active date and click
        // on 'Clear' button and see, that the day active before, now isn't active
        cy.get('input[type=text]').eq(0).type('10/02/2021')
        cy.get('.datepicker-days tr ').eq(5)
            .should('contain','10').children('td').eq(2)
            .should('have.class','active day')
        cy.get('.datepicker-days tr').eq(10)
            .should('contain','Clear').children('th')
            .click()
        cy.get('.input-group-addon').first().click()
        cy.get('.datepicker-days tr').eq(5)
            .should('contain','10').children('td').eq(2)
            .should('not.have.class','active day')
        cy.get('tr').eq(10)
            .should('contain','Clear').children('th')
            .click()
    })
    it('Test buttons of datepicker', () => {

        // I open the datepicker and click on 'previous','next' and 'date' buttons
        cy.get('.input-group-addon').first().click()
        cy.get('.datepicker-days')
        cy.get('tr').eq(1).children('th').eq(1)
            .contains('February 2021')
        cy.get('tr').eq(1).children('th').eq(0).click()
        cy.get('tr').eq(1).children('th').eq(1)
            .contains('January 2021')
        cy.get('tr').eq(1).children('th').eq(2).click()
        cy.get('tr').eq(1).children('th').eq(1)
            .contains('February 2021')

        cy.get('.input-group-addon').first().click()
        cy.get('.datepicker-days')
        cy.get('tr').eq(1)
            .children('th').eq(1).click()
        cy.get('.datepicker-months').eq(0)
        cy.get('tbody tr td span').eq(0)
            .click()
        cy.get('.datepicker-days')
        cy.get('tr').eq(1).children('th').eq(1)
            .contains('January 2021')
        cy.get('tr').eq(10)
            .should('contain','Clear').children('th')
            .click()
    })
    it('Test if week starts from Sunday and Sunday days disabled', () => {

        //I open the datepicker, and see, that week starts from Sunday  and
        // Sunday days disabled
        cy.get('.form-control').eq(1).click()
        cy.get('.datepicker-days tr').eq(2).children('th').eq(0)
            .should('contain','Su')
        cy.get('.form-control').eq(1).click()
        cy.get('.datepicker-days tr').eq(4).children('td').eq(0)
            .should('contain','7')
            .should('not.be.enabled')
        cy.get('.form-control').eq(2).click()
        cy.get('.datepicker-days')
        cy.get('tr').eq(2).children('th').eq(0)
            .should('contain','Su')
        cy.get('.form-control').eq(2).click()
        cy.get('.datepicker-days tr').eq(5).children('td').eq(0)
            .should('contain','14')
            .should(`not.be.enabled`)

        cy.get('.form-control').eq(1).click()
        cy.get('.datepicker-days tr').eq(5).children('td').eq(2)
            .should('contain','16').click()
        cy.get('.datepicker-days tr').eq(5).children('td').eq(2)
            .should('contain','16')
            .should('have.class','active selected range-start range-end day')
        cy.get('.form-control').eq(2).click()
        cy.get('.datepicker-days tr').eq(5).children('td').eq(2)
            .should('contain','16')
            .should('have.class','active selected range-start range-end day')


    })

})
